<?php

session_start();
    $_SESSION['message'] = "Welcome User";

    //connect to database  address  username  pw    database
    $conn = new mysqli("localhost", "root", "", "homework");
    // check connection

    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];  


           //$sql = "SELECT * FROM `User` WHERE UserName = '$username' AND Password = '$password'";
        $sql = "SELECT Username, Password FROM User"; 
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
           // echo "Name: " . $row["Username"]. " Password: " . $row["Password"]. "<br>"; // writes the sql database values
            if ($row["Username"] == $username && $row["Password"] == $password){
                $login = "Logging in";
               /* echo "
                    <script type='text/javascript'>
                    alert('$login');
                    window.location = '../mainPage/main.php';
                    </script>";*/
                    $_SESSION['message']=$login;
                    header('Location: ../fwd/main.html');
            }
            else if ($row["Username"] != $username || $row["Password"] != $password){
                $wrong = "Wrong username or password";
                /*echo 
                "<script type='text/javascript'>
                 alert('Wrong username or password');
                 window.location = './Index.html';
                </script>";*/
                $_SESSION['message'] = "$wrong";
                //header('Location: Index.html');
            }
        }
    } else {
       // echo "0 results";
       $_SESSION['message']='0 results';
    }

  $conn->close();
    }
session_abort();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index Page</title>
    <style>
        html {
            height: 100%;
        }
    body {
    text-align: center;
    font-family: sans-serif;
    text-align: center;
    text-align-all: center;
    background-image: linear-gradient(to bottom right, rgb(243, 181, 87), rgb(14, 16, 121));
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
}
#username, #pw, #pw2, #email{
    display: inline-flex;
    margin: 1%;
    width: 71%;
    height: 40px;
}
#signinBtn {
    margin: 1%;
    display: inline-block;
    width: 25%;
    height: 53px;
    line-height: 19px;
    text-align: center;
}

.loginForm {
    top: 50%;
    position: absolute;
    left: 50%;
    transform: translate(-50%,-50%);
    box-shadow: 0 0 20px 3px black;
    background-color: #fffcfc26;
}

div#message {
    font-weight: 200;
    padding-bottom: 15px;
    margin-bottom: 20px;
    color: #fbfbfb;
    box-shadow: 0px 0px 9px 0px black;
    margin-top: 2px;
    text-align: center;
    padding-top: 10px;
    background-image: linear-gradient(to bottom right, rgb(243, 181, 87), rgb(14, 16, 121));
    font-size: larger;
}

</style>


</head>
<body>
    <div class = "loginForm">
    <div id = "message"><?= $_SESSION['message'] ?></div>
        <form action="index1.php" method="POST">
                <input type="text" name="username" id="username" placeholder="Username" required="required">
                <input type="password" name="password" id="pw" placeholder="Password" required="required">
                <button type="submit" id="signinBtn">Sign In</button>
                <h3>Don't you have an account <a href="../signupPage/signup.php">Sign Up</a></aZ></h3>
        </form>
    </div> 
</body>
</html>