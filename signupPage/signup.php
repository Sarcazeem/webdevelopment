<?php
session_abort();
session_start();

$_SESSION['message'] = "Welcome User";

    //connect to database  address  username  pw    database
    $conn = new mysqli("localhost", "root", "", "homework");
    // check connection

    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }
if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password']) && isset($_POST['email'])) 
{ 

    $username = $_POST['username']; 
    $password = $_POST['password'];
    $password2 = $_POST['password2'];
    $email = $_POST['email'];

    $image = $_FILES['image']['name'];

    $uploaddir = 'images/';

    $uploadfile = $uploaddir . basename($_FILES['image']['name']);
    if ($password == $password2) {
        if(preg_match("!image!", $_FILES['image']['type'])){
            if($_FILES['image']['size'] < (1024*1024*10)){
                $user_check_query = "SELECT * FROM User WHERE UserName='$username' OR Email='$email' LIMIT 1";
                $result = mysqli_query($conn, $user_check_query);
                $user = mysqli_fetch_assoc($result);
                if ($user) { // if user exists
                    if ($user['UserName'] === $username) {
                        $_SESSION['message'] = "Username already exists";
                    }
    
                    if ($user['Email'] === $email) {
                        $_SESSION['message'] = "email already exists";
                    }
                }
                else{
                    $username = $_POST['username'];
                    if (move_uploaded_file($_FILES['image']['tmp_name'], __DIR__.'./images/'. $_FILES["image"]['name'])) {
                        // echo "Uploaded";
                        $_SESSION['username']=$username;
                        $_SESSION['image']=$uploadfile;
                        //create user
                        $sql = "INSERT INTO User (UserName, Password, Email, Coin, Picture) VALUES('$username', '$password', '$email', 10000, '$uploadfile')";
                        if ($conn->query($sql) === TRUE) {
                            $_SESSION['message'] = "Account has been created";
                        } else {
                            echo "Error: " . $sql . "<br>" . $conn->error;
                        }
                    }
                    else {
                        //echo "File was not uploaded";
                        $_SESSION['message']="File was not uploaded";
                    }
                } 
            }
            else{
                $_SESSION['message'] = "10MB Limit";
            }
        }
    }
    else{
       /* echo 
            "<script type='text/javascript'>
            alert('Password values does not match');
             window.location = './signup.html';
            </script>";*/
            $_SESSION['message'] = "Password values does not match";
    }

   // session_destroy();
  $conn->close();

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Signup Page</title>
    <script type='text/javascript'>
        function preview_image(event) 
        {
         var reader = new FileReader();
         reader.onload = function()
         {
          var output = document.getElementById('output_image');
          output.src = reader.result;
         }
         reader.readAsDataURL(event.target.files[0]);
        }
        </script>
        <style>
        html {
            height: 100%;
        }
        body{
    height: 100%;
    text-align: center;
    font-family: sans-serif;
    text-align: center;
    background-image: linear-gradient(to bottom right, rgb(243, 181, 87), rgb(14, 16, 121));
    background-size:     cover;                      
    background-repeat:   no-repeat;
    background-position: center;
}
#username, #pw, #pw2, #email{
    display: inline-flex;
    margin: 1% 0;
    width: 71%;
    height: 40px;
}
#username, #pw, #pw2, button{
    height: 40px;
}
#profileP {
    display: inline;
    margin: 1% 0;
    width: 71%;
    height: 30%;
    padding-right: 18%
}
#upload {
    display: inline-block;
    margin-left: 20px;
    float: right;
    width: 30%;
    height: 30%;
}

#signinBtn {
    display: inline-block;

}
#upload {
    float: right;
    padding-bottom: 5px;
}

.signinForm{
    width: 40%;
    margin: 0 auto;
    top: 50%;
    position: absolute;
    left: 50%;
    transform: translate(-50%,-50%);
}
.signinForm input{
    width: 100% !important;
}   

label {
    display: block;
    font: 1rem 'Fira Sans', sans-serif;
}

input,
label {
    margin: .4rem 0;
}

.note {
    font-size: .8em;
}


.signinForm button{
    margin: 1% 0;
    display: inline-flex;
    height: 52px;
    line-height: 28px;
    padding: 6px;
    width: 40%;
}
form{
    top: 50%;
    position: absolute;
    left: 50%;
    transform: translate(-50%,-50%);
    width: -webkit-fill-available;
    
}

div#message {
    font-weight: 200;
    padding-bottom: 15px;
    margin-bottom: 20px;
    color: #fbfbfb;
    box-shadow: 0px 0px 9px 0px black;
    margin-top: 2px;
    text-align: center;
    padding-top: 10px;
    background-image: linear-gradient(to bottom right, rgb(243, 181, 87), rgb(14, 16, 121));
    font-size: larger;
}

        </style>
</head>
<body>
    <div class = "signinForm">
    
        <form action="signup.php" method="POST" enctype="multipart/form-data">
                <div id = "message"><?= $_SESSION['message'] ?></div>
                <img id="output_image"/>
                <text style="float: left; padding-bottom: 5px;color: aquamarine">Upload Picture</text>
                <input type="file" accept="image/*" name="image"  required="required">
                <input type="text" name="username" id="username" placeholder="Username" required="required">
                <input type="password" name="password" id="pw" placeholder="Password" required="required">
                <input type="password" name="password2" id="pw2" placeholder="Password Again" required="required"> 
                <input type="email" name="email" id="email" placeholder="Email" required="required">
                <button type="submit" id="signinBtn">Sign In</button>
        </form>
    </div>
</body>
</html>